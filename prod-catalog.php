<?php include "header.php";?>

	<section class="top-home-section">
		<div class="container">
		<div class="row">
		<span class="pull-right">
				<ul class="list-inline pull-right" style="position: relative; top: 10px;">
                            <li class="dropdown">
                                <a href="#" title="keterangan icon" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-info-circle"></i> <span class="hidden-xs">Keterangan</span><i class="caret"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu" style="padding: 10px;">
                                    <li><i class="fa fa-thumbs-up"></i> Kondisi Baru</li>
                                    <li><i class="fa fa-subscript"></i> Kondisi Seken</li>
                                    <li class="divider"></li>                                    
                                    <li><i class="fa fa-male"></i> Untuk Laki-laki</li>
                                    <li><i class="fa fa-female"></i> Untuk Perempuan</li>
                                    <li><i class="fa fa-male"></i> Untuk Anak-anak</li>
                                    <li class="divider"></li>
                                    <li><i class="fa fa-scissors"></i> Diskon</li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" title="filter produk" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-filter"></i> <span class="hidden-xs">Filter</span><i class="caret"></i>
                                </a>
                                <div class="dropdown-menu dropdown-transparent" role="menu" style="min-width: 330px; left: -250px;">
                                    <div class="panel panel-warning">
                                        <div class="panel-heading">
                                            <div class="panel-title">Filter Produk</div>
                                        </div>
                                        <div class="panel-body">
                                            <form class="form-horizontal" role="form" accept-charset="utf-8" method="get" action="">
                                                                                                <div class="input-group input-group">
                                                    <span class="input-group-addon">Kondisi Produk:</span>
                                                    <select name="fc" class="form-control">
                                                        <option value="">Baru dan Seken</option>
                                                        <option value="baru">Baru</option>
                                                        <option value="seken">Seken</option>
                                                    </select>
                                                </div>
                                                <div class="input-group input-group">
                                                    <span class="input-group-addon">Produk Untuk :</span>
                                                    <select name="fde" class="form-control">
                                                        <option value="">Semua Orang</option>
                                                        <option value="l">Laki-laki</option>
                                                        <option value="p">Perempuan</option>
                                                        <option value="a">Anak-anak</option>
                                                    </select>
                                                </div>
                                                <div class="input-group input-group">
                                                    <span class="input-group-addon">Diskon Produk :</span>
                                                    <select name="fdi" class="form-control">
                                                        <option value="0">Diskon dan Non-Diskon</option>
                                                        <option value="1">Hanya yang diskon</option>
                                                    </select>
                                                </div>
                                                <button type="submit" class="form-control btn btn-warning">Apply</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
			</span>
			<h1>
				<i class="fa fa-shopping-cart"></i>
				Perkebunan
				<small>Katalog Produk Yukbisnis</small>
			</h1>
			
			<ol class="breadcrumb">
	            <li><a href="http://yukbisnis.com/">Home</a></li>
	            <li class="active">Katalog</li>
	            <li><a href="http://yukbisnis.com/katalog/agro-bisnis">Agro Bisnis</a></li>
	            <li class="active">Perkebunan</li>
		    </ol>
	        <hr/>
		</div>
	    <div class="row">	        
				
		    <div class="col-xs-4 col-sm-3 col-md-2">
		            <div class="thumbnail prod-catalog">
		                <img src="img/hijab.jpg">
		                <a href="#" class="col-xs-12 text-center" role="button">
							<strong>Rok Panjang Katun</strong>
							<br>
							<span class="text-danger">Rp. 50.000,-</span>
		                </a>
		                <div class="clearfix"></div>
		            </div>
		        </div>
		        <div class="col-xs-4 col-sm-3 col-md-2">
		            <div class="thumbnail prod-catalog">
		                <img src="img/hijab.jpg">
		                <a href="#" class="col-xs-12 text-center" role="button">
							<strong>Rok Panjang Katun</strong>
							<br>
							<span class="text-danger">Rp. 50.000,-</span>
		                </a>
		                <div class="clearfix"></div>
		            </div>
		        </div>
		        <div class="col-xs-4 col-sm-3 col-md-2">
		            <div class="thumbnail prod-catalog">
		                <img src="img/hijab.jpg">
		                <a href="#" class="col-xs-12 text-center" role="button">
							<strong>Rok Panjang Katun</strong>
							<br>
							<span class="text-danger">Rp. 50.000,-</span>
		                </a>
		                <div class="clearfix"></div>
		            </div>
		        </div>
		        <div class="col-xs-4 col-sm-3 col-md-2">
		            <div class="thumbnail prod-catalog">
		                <img src="img/hijab.jpg">
		                <a href="#" class="col-xs-12 text-center" role="button">
							<strong>Rok Panjang Katun</strong>
							<br>
							<span class="text-danger">Rp. 50.000,-</span>
		                </a>
		                <div class="clearfix"></div>
		            </div>
		        </div>
		        <div class="col-xs-4 col-sm-3 col-md-2">
		            <div class="thumbnail prod-catalog">
		                <img src="img/hijab.jpg">
		                <a href="#" class="col-xs-12 text-center" role="button">
							<strong>Rok Panjang Katun</strong>
							<br>
							<span class="text-danger">Rp. 50.000,-</span>
		                </a>
		                <div class="clearfix"></div>
		            </div>
		        </div>
		        <div class="col-xs-4 col-sm-3 col-md-2">
		            <div class="thumbnail prod-catalog">
		                <img src="img/hijab.jpg">
		                <a href="#" class="col-xs-12 text-center" role="button">
							<strong>Rok Panjang Katun</strong>
							<br>
							<span class="text-danger">Rp. 50.000,-</span>
		                </a>
		                <div class="clearfix"></div>
		            </div>
		        </div>
		        <div class="col-xs-4 col-sm-3 col-md-2">
		            <div class="thumbnail prod-catalog">
		                <img src="img/hijab.jpg">
		                <a href="#" class="col-xs-12 text-center" role="button">
							<strong>Rok Panjang Katun</strong>
							<br>
							<span class="text-danger">Rp. 50.000,-</span>
		                </a>
		                <div class="clearfix"></div>
		            </div>
		        </div>
		        <div class="col-xs-4 col-sm-3 col-md-2">
		            <div class="thumbnail prod-catalog">
		                <img src="img/hijab.jpg">
		                <a href="#" class="col-xs-12 text-center" role="button">
							<strong>Rok Panjang Katun</strong>
							<br>
							<span class="text-danger">Rp. 50.000,-</span>
		                </a>
		                <div class="clearfix"></div>
		            </div>
		        </div>
		        <div class="col-xs-4 col-sm-3 col-md-2">
		            <div class="thumbnail prod-catalog">
		                <img src="img/hijab.jpg">
		                <a href="#" class="col-xs-12 text-center" role="button">
							<strong>Rok Panjang Katun</strong>
							<br>
							<span class="text-danger">Rp. 50.000,-</span>
		                </a>
		                <div class="clearfix"></div>
		            </div>
		        </div>
		        <div class="col-xs-4 col-sm-3 col-md-2">
		            <div class="thumbnail prod-catalog">
		                <img src="img/hijab.jpg">
		                <a href="#" class="col-xs-12 text-center" role="button">
							<strong>Rok Panjang Katun</strong>
							<br>
							<span class="text-danger">Rp. 50.000,-</span>
		                </a>
		                <div class="clearfix"></div>
		            </div>
		        </div>
		        <div class="col-xs-4 col-sm-3 col-md-2">
		            <div class="thumbnail prod-catalog">
		                <img src="img/hijab.jpg">	
		                <a href="#" class="col-xs-12 text-center" role="button">
							<strong>Rok Panjang Katun</strong>
							<br>
							<span class="text-danger">Rp. 50.000,-</span>
		                </a>
		                <div class="clearfix"></div>
		            </div>
		        </div>
		        <div class="col-xs-4 col-sm-3 col-md-2">
		            <div class="thumbnail prod-catalog">
		                <img src="img/hijab.jpg">
		                <a href="#" class="col-xs-12 text-center" role="button">
							<strong>Rok Panjang Katun</strong>
							<br>
							<span class="text-danger">Rp. 50.000,-</span>
		                </a>
		                <div class="clearfix"></div>
		            </div>
		        </div>
		</div>
		<div class="row text-center">
			<ul class="pagination pagination-md"><li class="active"><a href="">1</a></li><li class="page"><a class="follow_link" href="http://yukbisnis.com/katalog/agro-bisnis/perkebunan/2">2</a></li><li class="page"><a class="follow_link" href="http://yukbisnis.com/katalog/agro-bisnis/perkebunan/3">3</a></li><li class="next page"><a class="follow_link" href="http://yukbisnis.com/katalog/agro-bisnis/perkebunan/2">Next →</a></li></ul>
		</div>
	</div>
	</section>

<?php include "footer.php"; ?>