

	<footer>
		<section id="bottom">
	        <div class="container">
	            <div class="row">
	                <div class="col-md-3 col-sm-6">
	                    <h4>About Us</h4>
	                    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p>
	                    <p>Pellentesque habitant morbi tristique senectus.</p>
	                </div><!--/.col-md-3-->

	                <div class="col-md-3 col-sm-6">
	                    <h4>Company</h4>
	                    <div>
	                        <ul class="arrow">
	                            <li><a href="#">The Company</a></li>
	                            <li><a href="#">Our Team</a></li>
	                            <li><a href="#">Our Partners</a></li>
	                            <li><a href="#">Our Services</a></li>
	                            <li><a href="#">Faq</a></li>
	                            <li><a href="#">Conatct Us</a></li>
	                            <li><a href="#">Privacy Policy</a></li>
	                            <li><a href="#">Terms of Use</a></li>
	                            <li><a href="#">Copyright</a></li>
	                        </ul>
	                    </div>
	                </div><!--/.col-md-3-->

	                <div class="col-md-3 col-sm-6">
	                    <h4>Latest Blog</h4>
	                    <div>
	                        <div class="media">
	                            <div class="pull-left">
	                            </div>
	                            <div class="media-body">
	                                <span class="media-heading"><a href="#">Pellentesque habitant morbi tristique senectus</a></span>
	                                <small class="muted">Posted 17 Aug 2013</small>
	                            </div>
	                        </div>
	                        <div class="media">
	                            <div class="pull-left">
	                            </div>
	                            <div class="media-body">
	                                <span class="media-heading"><a href="#">Pellentesque habitant morbi tristique senectus</a></span>
	                                <small class="muted">Posted 13 Sep 2013</small>
	                            </div>
	                        </div>
	                        <div class="media">
	                            <div class="pull-left">
	                            </div>
	                            <div class="media-body">
	                                <span class="media-heading"><a href="#">Pellentesque habitant morbi tristique senectus</a></span>
	                                <small class="muted">Posted 11 Jul 2013</small>
	                            </div>
	                        </div>
	                    </div>  
	                </div><!--/.col-md-3-->

	                <div class="col-md-3 col-sm-6">
	                    <h4>Address</h4>
	                    <address>
	                        <strong>Twitter, Inc.</strong><br>
	                        795 Folsom Ave, Suite 600<br>
	                        San Francisco, CA 94107<br>
	                        <abbr title="Phone">P:</abbr> (123) 456-7890
	                    </address>
	                    <h4>Newsletter</h4>
	                    <form role="form">
	                        <div class="input-group">
	                            <input type="text" class="form-control" autocomplete="off" placeholder="Enter your email">
	                            <span class="input-group-btn">
	                                <button class="btn btn-danger" type="button">Go!</button>
	                            </span>
	                        </div>
	                    </form>
	                </div> <!--/.col-md-3-->
	            </div>
	        </div>
	    </section>

		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-12">
		
					<p>Copyright &copy; 2014 YukBisnis </p>
				</div>
			</div>	
		</div>
	</footer>

    <!-- Core JavaScript Files -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/classie.js"></script>
	<script src="js/jquery.scrollTo.js"></script>
	<script src="js/nivo-lightbox.min.js"></script>
	<script src="js/stellar.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="js/custom.js"></script>

    <!-- Search Modal -->
	<div class="modal fade" id="pencarian" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title" id="myModalLabel">Pencarian</h4>
	      </div>
	      <div class="modal-body">
	        <form name="fsearch" class="form" role="search" accept-charset="utf-8" method="get" action="http://yukbisnis.com/datamember">
	            <select name="type" onchange="ubah(this.value)" class="form-control">
	                <option value="item">Produk/Jasa</option>
	                <option value="store">Toko</option>
	                <option value="user">Member</option>
	            </select>                                 
	            <input name="kw" type="text" class="form-control" placeholder="Kata Kunci">                
	        </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-primary"><i class="fa fa-search"></i> Cari</button>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Login Modal -->
	<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title" id="myModalLabel">Login</h4>
	      </div>
	      <div class="modal-body">
	         <form role="form">
		        <div class="form-group">
		          <label for="inputUsernameEmail">Alamat Email</label>
		          <input type="text" class="form-control" id="inputUsernameEmail">
		        </div>
		        <div class="form-group">
		          <a class="pull-right" href="#">Lupa password?</a>
		          <label for="inputPassword">Password</label>
		          <input type="password" class="form-control" id="inputPassword">
		        </div>
		        <div class="checkbox pull-right">
		          <label>
		            <input type="checkbox">
		            Selalu Ingat Saya </label>
		        </div>
		        <button type="submit" class="btn btn btn-primary">
		          Log In
		        </button>
		      </form>
	      </div>
	    </div>
	  </div>
	</div>

</body>

</html>
