<?php include "header.php";?>

	<section class="top-home-section">
		<div class="container">
		<div class="row">
			<h1 class="text-center">Yubipay
			<br>
				<small>Panduan Lengkap Yubipay</small>
			</h1>
	        <hr/>
		</div>
	    <div class="row">	        
		<div class="well">
	        <h3><strong>Apa Itu YubiPay?</strong></h3>
			<p>YubiPay adalah layanan rakening bersama dari YukBisnis.Com yang 
			bertujuan untuk membuat transaksi lebih nyaman dan meningkatkan 
			kepercayaan toko online di YukBisnis.Com.</p>
			<p>&nbsp;</p>
			<h3><strong>Kenapa Toko Online Saya Harus Menggunakan YubiPay?</strong></h3>
			<p>Ada beberapa kelebihan yang akan Anda dapatkan jika Anda bertransaksi menggunakan YubiPay:</p>
			<p>1.&nbsp;<strong>AMAN – </strong>Setiap transaksi pembayaran pembeli 
			akan ditransfer ke rekening Yubi dahulu, hingga barang diterima tanpa 
			komplain. Hal ini akan menarik trafik pembeli lebih banyak pastinya.</p>
			<p>2. <strong>MUDAH -</strong>&nbsp;Tidak ribet (maaf) seperti rekber 
			lainnya, YubiPay tidak akan merepotkan pembeli dengan effort seperti 
			scan/email/fax bukti transfer ke rekber. Tim YubiPay akan melakukan 
			pengecekan rekening tiap 3 jam.</p>
			<p><strong>3. TERDETEKSI -</strong>&nbsp;Yubi akan memberi laporan 
			berkala tentang transaksi, pembayaran, pengiriman produk via SMS, pesan 
			dan email, baik ke pembeli maupun penjual.</p>
			<p>4. <strong>GRATIS -</strong>&nbsp;Seluruh layanan YubiPay GRATIS, 
			alias TIDAK BAYAR, TIDAK ADA FEE, atau PEMOTONGAN APAPUN. Harga jual 
			Anda bisa tetap kompetitif, kami mengerti persaingan bisnis online.</p>
			<p>&nbsp;</p>
			<h3><strong>Bagaimana Proses Pembayaran Menggunakan YubiPay?</strong></h3>
			<p><strong>Langkah Pertama</strong></p>
			<p>Pembeli berbelanja di toko bertanda YubiPay ready</p>
			<p>Anda bisa melihat daftar toko yang sudah siap bertransaksi menggunakan YubiPay <a href="http://www.yukbisnis.com/katalog/yubipay/data_list_yubipay" target="_blank">disini</a>.</p>
			<p>&nbsp;</p>
			<div>
				<p><strong>Langkah Kedua</strong></p>
				<p>Pembeli kemudian melakukan kesepakatan dengan penjual untuk bertransaksi menggunakan YubiPay.</p>
				<p style="text-align: left;"><strong>Langkah Ketiga</strong></p>
				<p style="text-align: left;">Pembeli kemudian meng-klik tombol “Yuk 
				beli” yang ada dibagian bawah foto produk. Lalu mengikuti proses 
				pembelian. Pembeli mengisi data lengkap (identitas, alamat, dll.) Namun 
				jika pembeli sudah terdaftar di Yubi, maka data tersebut sudah secara 
				otomatis akan terisi.</p>
				<p style="text-align: left;"><strong>Langkah Keempat</strong></p>
				<p style="text-align: left;">Pada bagian akhir proses pembayaran, 
				pembeli akan melihat nomor rekening bersama dimana pembeli dapat 
				mentransfer kesana. Pembeli akan menerima invoice pembelian melalui 
				email. Ini dia nomor rekening bersama Yubi yang akan Anda terima:</p>
				<p style="text-align: left;"><strong>Langkah Kelima</strong></p>
				<p style="text-align: left;">Tim Yubi akan memberikan konfirmasi kepada penjual bahwa penjual sudah bisa mengirimkan produknya &nbsp;ke pembeli.</p>
				<p style="text-align: left;"><strong>Langkah Keenam</strong></p>
				<p style="text-align: left;">Penjual mengirimkan produknya kepada pembeli.</p>
				<p style="text-align: left;"><strong>Langkah ketujuh</strong></p>
				<p style="text-align: left;">Pembeli menerima produk dan mengkonfirmasi kepada tim Yubi bahwa dana-nya sudah bisa diransfer ke penjual.</p>
				<p style="text-align: left;"><strong>Langkah Kedelapan</strong></p>
				<p style="text-align: left;">Tim Yubi mentransfer dana pembayaran pembeli ke penjual.</p>
				<p style="text-align: left;"><strong>Langkah Kesembilan</strong></p>
				<p style="text-align: left;">Pembeli dan penjual bahagia selama-lamanya.</p>
				<p>&nbsp;</p>
				<h3 style="text-align: left;"><strong>Bagaimana Caranya Agar Toko Saya Bisa Masuk Dalam Daftar YubiPay Ready?</strong></h3>
				<p style="text-align: left;">Untuk sementara, YubiPay masih dalam tahap 
				ujicoba sehingga kami akan menyeleksi dan menerima hanya 100 toko. 
				Sampai saat ini kami masih intensif menyeleksi toko-toko yang akan bisa 
				menggunakan fitur YubiPay. Jika Anda ingin menggunakan YubiPay dalam 
				toko Anda, berikut ini adalah syarat-syarat yang harus Anda penuhi:</p>
				<p style="text-align: left;">1.Foto profil asli &amp; Logo bisnis yang benar.</p>
				<p style="text-align: left;">2. Identitas, alamat lengkap, nomor HP yang dapat dihubungi (tim Yubi akan menghubungi Anda untuk konfirmasi,) email.</p>
				<p style="text-align: left;">3. Anda telah meng-upload minimal 10 produk kedalam toko online Anda.</p>
				<p style="text-align: left;">4. Produk yang Anda jual bukan produk ilegal.</p>
				<p style="text-align: left;">5. Gambar produk bagus dan sesuai. Deskripsi produk lengkap.</p>
				<p style="text-align: left;">6. Bersedia free ongkir untuk daerah Jabodetabek dan Bandung via JNE.</p>
				<p>&nbsp;</p>
				<h3 style="text-align: left;">Bagaimana Mengaktifkan Fitur YubiPay Untuk Toko Saya?</h3>
				<p>Berikut ini adalah tahapan untuk mengaktifkan fitur YubiPay dalam toko Anda di YukBisnis.</p>
				<p>1. Setelah Anda login, klik “Bisnisku” pada menu bagian atas.</p>
				<p style="text-align: left;">2. Lalu klik “Bank” pada menu disebelah kiri.</p>
				<p style="text-align: left;">3. Isilah data rekening Anda.&nbsp;Untuk sementara kami hanya menerima rekening dari Bank BCA dan Mandiri.</p>
				<p style="text-align: left;">4. Cek dan ricek kembali data-data Anda. Setelah selesai klik “Yuk” dan tunggu email konfirmasi dari tim Yubi.</p>                    
			</div>
        </div>
	        
	    </div>
	</div>
	</section>

<?php include "footer.php"; ?>