<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>YukBisnis Moya</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap-red.css" rel="stylesheet" type="text/css">

    <!-- Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="css/nivo-lightbox.css" rel="stylesheet" />
	<link href="css/nivo-lightbox-theme/default/default.css" rel="stylesheet" type="text/css" />
	<link href="css/animate.css" rel="stylesheet" />
    <!-- CSS -->
    <link href="css/style-red.css" rel="stylesheet">

</head>

<body data-spy="scroll">

<div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <form class="navbar-form" role="search">
            <a href="index.php">
              <img src="img/logo-red.png" alt="" style="height: 70px; padding:10px;">
            </a>
                <div class="input-group input-md hidden-xs hidden-sm">
               <input type="text" class="form-control"  style="width: 280px" placeholder="Cari Disini">
               <div class="input-group-btn">
                  <select class="form-control dropdown-select">
                    <option value="one">Produk/Jasa</option>
                    <option value="two">Toko</option>
                    <option value="three">Member</option>
                </select>
                <button class="form-control btn btn-success"><i class="fa fa-search"></i></button>
               </div><!-- /btn-group -->
            </div><!-- /input-group -->
      
            </div>
            </form>
        <div class="navbar-collapse collapse navbar-right">
          <ul class="nav navbar-nav">
      			<li> <a href="#"><i class="fa fa-fire"></i>  Hot</a></li>
      			<li> <a href="register.php"> <i class="fa fa-pencil-square-o"></i> Daftar</a></li>
      			<li> <a href="#" data-toggle="modal" data-target="#login"><i class="fa fa-lock"></i>  Login</a></li>
      			<li><a href="#" data-toggle="modal" data-target="#pencarian" class="hidden-lg hidden-md"><i class="fa fa-search"></i> <span class="visible-xs-inline">Pencarian</span></a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>