<?php include "header.php";?>

	<!-- Section: intro -->
    <section class="slider-section">
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="img/banner-buka-botol.jpg" alt="...">
    </div>
    <div class="item">
      <img src="img/banner-buka-botol.jpg" alt="...">
    </div>
    <div class="item">
      <img src="img/banner-buka-botol.jpg" alt="...">
    </div>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
    </section>
	<!-- /Section: intro -->

	<section class="home-section">
		<div class="container">
		<div class="row">
			
		</div>
		<div class="row col-md-3">
		<h1>Produk Terbaru <a href="#" class="btn btn-warning btn-xs">Selengkapnya <i class="fa fa-arrow-right"></i></a></h1>
			<ul class="nav nav-pills nav-stacked">
			  <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Contoh Dropdown <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                  <li class="dropdown-header">Kategori Produk</li>
                  <li class="dropdown-submenu">
	                <a tabindex="-1" href="#">Contoh Submenu Multilevel</a>
	                <ul class="dropdown-menu">
	                  <li><a tabindex="-1" href="#">Second level</a></li>
	                  <li class="dropdown-submenu">
	                    <a href="#">Even More..</a>
	                    <ul class="dropdown-menu">
	                        <li><a href="#">3rd level</a></li>
	                    	<li><a href="#">3rd level</a></li>
	                    </ul>
	                  </li>
	                  <li><a href="#">Second level</a></li>
	                  <li><a href="#">Second level</a></li>
	                </ul>
	              </li>
                </ul>
              </li>
              <li><a href="#">Agro Bisnis</a></li>
              <li><a href="#">Elektronik &amp; HP</a></li>
              <li><a href="#">Fashion</a></li>
              <li><a href="#">Handicrafts</a></li>
              <li><a href="#">Hobi &amp; Hiburan</a></li>
              <li><a href="#">Jasa</a></li>
              <li><a href="#">Kantor &amp; Industri</a></li>
              <li><a href="#">Kesehatan</a></li>
              <li><a href="#">Komputer</a></li>
              <li><a href="#">Kuliner</a></li>
              <li><a href="#">Otomotif</a></li>
              <li><a href="#">Perlengkapan Rumah</a></li>
			</ul>
		</div>
	    <div class="row col-md-9" style="padding-left: 20px;">
	        <div class="col-xs-4 col-sm-3 col-md-3">
	            <div class="thumbnail">
	                <img src="img/hijab.jpg">
	                <a href="#" class="btn btn-warning col-xs-12" role="button">
						<strong>Rok Panjang Katun</strong>
						<br>
						Rp. 50.000,-
	                </a>
	                <div class="clearfix"></div>
	            </div>
	        </div>
	        <div class="col-xs-4 col-sm-3 col-md-3">
	            <div class="thumbnail">
	                <img src="img/hijab.jpg">
	                <a href="#" class="btn btn-warning col-xs-12" role="button">
						<strong>Rok Panjang Katun</strong>
						<br>
						Rp. 50.000,-
	                </a>
	                <div class="clearfix"></div>
	            </div>
	        </div>
	        <div class="col-xs-4 col-sm-3 col-md-3">
	            <div class="thumbnail">
	                <img src="img/hijab.jpg">
	                <a href="#" class="btn btn-warning col-xs-12" role="button">
						<strong>Rok Panjang Katun</strong>
						<br>
						Rp. 50.000,-
	                </a>
	                <div class="clearfix"></div>
	            </div>
	        </div>
	        <div class="col-xs-4 col-sm-3 col-md-3">
	            <div class="thumbnail">
	                <img src="img/hijab.jpg">
	                <a href="#" class="btn btn-warning col-xs-12" role="button">
						<strong>Rok Panjang Katun</strong>
						<br>
						Rp. 50.000,-
	                </a>
	                <div class="clearfix"></div>
	            </div>
	        </div>
	        <div class="col-xs-4 col-sm-3 col-md-3">
	            <div class="thumbnail">
	                <img src="img/hijab.jpg">
	                <a href="#" class="btn btn-warning col-xs-12" role="button">
						<strong>Rok Panjang Katun</strong>
						<br>
						Rp. 50.000,-
	                </a>
	                <div class="clearfix"></div>
	            </div>
	        </div>
	        <div class="col-xs-4 col-sm-3 col-md-3">
	            <div class="thumbnail">
	                <img src="img/hijab.jpg">
	                <a href="#" class="btn btn-warning col-xs-12" role="button">
						<strong>Rok Panjang Katun</strong>
						<br>
						Rp. 50.000,-
	                </a>
	                <div class="clearfix"></div>
	            </div>
	        </div>
	        <div class="col-xs-4 col-sm-3 col-md-3">
	            <div class="thumbnail">
	                <img src="img/hijab.jpg">
	                <a href="#" class="btn btn-warning col-xs-12" role="button">
						<strong>Rok Panjang Katun</strong>
						<br>
						Rp. 50.000,-
	                </a>
	                <div class="clearfix"></div>
	            </div>
	        </div>
	        <div class="col-xs-4 col-sm-3 col-md-3">
	            <div class="thumbnail">
	                <img src="img/hijab.jpg">
	                <a href="#" class="btn btn-warning col-xs-12" role="button">
						<strong>Rok Panjang Katun</strong>
						<br>
						Rp. 50.000,-
	                </a>
	                <div class="clearfix"></div>
	            </div>
	        </div>
	        <div class="col-xs-4 col-sm-3 col-md-3">
	            <div class="thumbnail">
	                <img src="img/hijab.jpg">
	                <a href="#" class="btn btn-warning col-xs-12" role="button">
						<strong>Rok Panjang Katun</strong>
						<br>
						Rp. 50.000,-
	                </a>
	                <div class="clearfix"></div>
	            </div>
	        </div>
	        <div class="col-xs-4 col-sm-3 col-md-3">
	            <div class="thumbnail">
	                <img src="img/hijab.jpg">
	                <a href="#" class="btn btn-warning col-xs-12" role="button">
						<strong>Rok Panjang Katun</strong>
						<br>
						Rp. 50.000,-
	                </a>
	                <div class="clearfix"></div>
	            </div>
	        </div>
	        <div class="col-xs-4 col-sm-3 col-md-3">
	            <div class="thumbnail">
	                <img src="img/hijab.jpg">
	                <a href="#" class="btn btn-warning col-xs-12" role="button">
						<strong>Rok Panjang Katun</strong>
						<br>
						Rp. 50.000,-
	                </a>
	                <div class="clearfix"></div>
	            </div>
	        </div>
	        <div class="col-xs-4 col-sm-3 col-md-3">
	            <div class="thumbnail">
	                <img src="img/hijab.jpg">
	                <a href="#" class="btn btn-warning col-xs-12" role="button">
						<strong>Rok Panjang Katun</strong>
						<br>
						Rp. 50.000,-
	                </a>
	                <div class="clearfix"></div>
	            </div>
	        </div>
	    </div>
	</div>
	</section>

	<section class="home-section bg-gray">
		<div class="container">
		    <div class="row col-md-12">
		    <h1>Bisnis Populer <a href="#" class="btn btn-warning btn-xs">Selengkapnya <i class="fa fa-arrow-right"></i></a></h1>
		        <hr/>
		        <div class="col-xs-6 col-sm-4 col-md-3">
		            <div class="thumbnail">
		                <img src="img/toko.jpg">
		                <a href="#" class="btn btn-warning col-xs-12" role="button">
							Pink Panda Shop
		                </a>
		                <div class="clearfix"></div>
		            </div>
		        </div>
		        <div class="col-xs-6 col-sm-4 col-md-3">
		            <div class="thumbnail">
		                <img src="img/toko2.jpg">
		                <a href="#" class="btn btn-warning col-xs-12" role="button">
							Uaena Store
		                </a>
		                <div class="clearfix"></div>
		            </div>
		        </div>
		        <div class="col-xs-6 col-sm-4 col-md-3">
		            <div class="thumbnail">
		                <img src="img/toko.jpg">
		                <a href="#" class="btn btn-warning col-xs-12" role="button">
							Pink Panda Shop
		                </a>
		                <div class="clearfix"></div>
		            </div>
		        </div>
		        <div class="col-xs-6 col-sm-4 col-md-3">
		            <div class="thumbnail">
		                <img src="img/toko2.jpg">
		                <a href="#" class="btn btn-warning col-xs-12" role="button">
							Uaena Store
		                </a>
		                <div class="clearfix"></div>
		            </div>
		        </div>
		    </div>
	</div>
	</section>

	<section class="home-section">
		<div class="container">
		    <div class="row col-md-8">
		    <h1>Berita Hot <a href="#" class="btn btn-warning btn-xs">Selengkapnya <i class="fa fa-arrow-right"></i></a></h1>
		    <hr/>

			    <h3><a href="#">Tutorial Toko Online: Menggunakan Fitur Tema Di YukBisnis.Com</a></h3>
			    <p>Baru-baru ini, YukBisnis.Com meluncurkan fitur terbaru dalam pembuatan toko online yaitu FITUR TEMA yang akan memperindah tampilan toko para pengguna jasa YukBisnis. Berikut adalah tutorial penggunaan FITUR TEMA untuk Toko Online tersebut. Selamat Mencoba ! 1. Masuklah ke alamat YukBisnis.com</p>
			    <div>
			        <span class="label label-success">Posted 2012-08-02 20:47:04</span>
			        <div class="pull-right">
				        <span class="label label-warning">Tutorial</span>
				        <span class="label label-warning">Berita</span>
			        </div>
			    </div>     
			    <hr>

			    <h3><a href="#">Tutorial Toko Online: Menggunakan Fitur Tema Di YukBisnis.Com</a></h3>
			    <p>Baru-baru ini, YukBisnis.Com meluncurkan fitur terbaru dalam pembuatan toko online yaitu FITUR TEMA yang akan memperindah tampilan toko para pengguna jasa YukBisnis. Berikut adalah tutorial penggunaan FITUR TEMA untuk Toko Online tersebut. Selamat Mencoba ! 1. Masuklah ke alamat YukBisnis.com</p>
			    <div>
			        <span class="label label-success">Posted 2012-08-02 20:47:04</span>
			        <div class="pull-right">
				        <span class="label label-warning">Tutorial</span>
				        <span class="label label-warning">Berita</span>
			        </div>
			    </div>     
			    <hr>

			    <h3><a href="#">Tutorial Toko Online: Menggunakan Fitur Tema Di YukBisnis.Com</a></h3>
			    <p>Baru-baru ini, YukBisnis.Com meluncurkan fitur terbaru dalam pembuatan toko online yaitu FITUR TEMA yang akan memperindah tampilan toko para pengguna jasa YukBisnis. Berikut adalah tutorial penggunaan FITUR TEMA untuk Toko Online tersebut. Selamat Mencoba ! 1. Masuklah ke alamat YukBisnis.com</p>
			    <div>
			        <span class="label label-success">Posted 2012-08-02 20:47:04</span>
			        <div class="pull-right">
				        <span class="label label-warning">Tutorial</span>
				        <span class="label label-warning">Berita</span>
			        </div>
			    </div>     
			    <hr>

		    </div>
		    <div class="row col-md-4" style="padding-left: 40px;">
		    <h1>Pilihan</h1>
		    <hr/>
		    	<ul class="nav nav-tabs" role="tablist" id="myTab">
					<li class="active"><a href="#terbaru" role="tab" data-toggle="tab">Terbaru</a></li>
					<li><a href="#terlaris" role="tab" data-toggle="tab">Terlaris</a></li>
					<li><a href="#harga-coret" role="tab" data-toggle="tab">Harga Coret</a></li>
				</ul>

				<div class="tab-content">
					<div class="tab-pane active" id="terbaru">
					<br>
						<div class="media">
					      <a class="pull-left" href="#">
					        <img class="media-object" src="http://placehold.it/64x64" style="width: 64px; height: 64px;">
					      </a>
					      <div class="media-body">
					        <h5 class="media-heading"><a href="#">Paket Reseller Modal BlackBerrry</a>
						        <br>
						        <small>
						        <s>Rp. 50.000</s> <span class="text-danger">Rp. 25.000</span>	
						        </small>
					        </h5>
					        
					      </div>
					    </div>
					    <div class="media">
					      <a class="pull-left" href="#">
					        <img class="media-object" src="http://placehold.it/64x64" style="width: 64px; height: 64px;">
					      </a>
					      <div class="media-body">
					        <h5 class="media-heading"><a href="#">Paket Reseller Modal BlackBerrry</a>
						        <br>
						        <small>
						        <s>Rp. 50.000</s> <span class="text-danger">Rp. 25.000</span>	
						        </small>
					        </h5>
					        
					      </div>
					    </div>
					    <div class="media">
					      <a class="pull-left" href="#">
					        <img class="media-object" src="http://placehold.it/64x64" style="width: 64px; height: 64px;">
					      </a>
					      <div class="media-body">
					        <h5 class="media-heading"><a href="#">Paket Reseller Modal BlackBerrry</a>
						        <br>
						        <small>
						        <s>Rp. 50.000</s> <span class="text-danger">Rp. 25.000</span>	
						        </small>
					        </h5>
					        
					      </div>
					    </div>
					    <div class="media">
					      <a class="pull-left" href="#">
					        <img class="media-object" src="http://placehold.it/64x64" style="width: 64px; height: 64px;">
					      </a>
					      <div class="media-body">
					        <h5 class="media-heading"><a href="#">Paket Reseller Modal BlackBerrry</a>
						        <br>
						        <small>
						        <s>Rp. 50.000</s> <span class="text-danger">Rp. 25.000</span>	
						        </small>
					        </h5>
					        
					      </div>
					    </div>
					    <div class="media">
					      <a class="pull-left" href="#">
					        <img class="media-object" src="http://placehold.it/64x64" style="width: 64px; height: 64px;">
					      </a>
					      <div class="media-body">
					        <h5 class="media-heading"><a href="#">Paket Reseller Modal BlackBerrry</a>
						        <br>
						        <small>
						        <s>Rp. 50.000</s> <span class="text-danger">Rp. 25.000</span>	
						        </small>
					        </h5>
					        
					      </div>
					    </div>
					    <div class="media">
					      <a class="pull-left" href="#">
					        <img class="media-object" src="http://placehold.it/64x64" style="width: 64px; height: 64px;">
					      </a>
					      <div class="media-body">
					        <h5 class="media-heading"><a href="#">Paket Reseller Modal BlackBerrry</a>
						        <br>
						        <small>
						        <s>Rp. 50.000</s> <span class="text-danger">Rp. 25.000</span>	
						        </small>
					        </h5>
					        
					      </div>
					    </div>
					    <div class="media">
					      <a class="pull-left" href="#">
					        <img class="media-object" src="http://placehold.it/64x64" style="width: 64px; height: 64px;">
					      </a>
					      <div class="media-body">
					        <h5 class="media-heading"><a href="#">Paket Reseller Modal BlackBerrry</a>
						        <br>
						        <small>
						        <s>Rp. 50.000</s> <span class="text-danger">Rp. 25.000</span>	
						        </small>
					        </h5>
					        
					      </div>
					    </div>
					    <div class="media">
					      <a class="pull-left" href="#">
					        <img class="media-object" src="http://placehold.it/64x64" style="width: 64px; height: 64px;">
					      </a>
					      <div class="media-body">
					        <h5 class="media-heading"><a href="#">Paket Reseller Modal BlackBerrry</a>
						        <br>
						        <small>
						        <s>Rp. 50.000</s> <span class="text-danger">Rp. 25.000</span>	
						        </small>
					        </h5>
					        
					      </div>
					    </div>

					</div>
					<div class="tab-pane" id="terlaris">...</div>
					<div class="tab-pane" id="harga-coret">...</div>
				</div>

				<script>
					$(function () {
					$('#myTab a:last').tab('show')
					})
				</script>
		    </div>
	</div>
	</section>
	
<?php include "footer.php";?>