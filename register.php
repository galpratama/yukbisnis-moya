<?php include "header.php";?>

	<section class="top-home-section">
		<div class="container">
		<div class="row">
			<h1 class="text-center">Daftar
			
				<small>Buka Bisnis di Yukbisnis</small>
			</h1>
	        <hr/>
		</div>
	    <div class="row">
			<div>                        
			    <form id="form-login" class="form-horizontal" role="form" accept-charset="utf-8" method="post" action="http://yukbisnis.com/register/store">
			        <div class="row">
			            <div class="col-md-8 col-md-offset-2">
			                <legend>Data Bisnis</legend>
			                <div class="control-group">
			                    <label>Nama Domain</label>
			                    <div id="hasilDomain"></div>
			                    <div class="input-group">
			                        <span class="input-group-addon"><i class="fa fa-globe"></i></span>
			                        <input id="nama_bisnis" name="nama_bisnis" value="p" type="text" class="form-control" required="">  
			                        <span class="input-group-addon">.yukbisnis.com</span>
			                        <span class="input-group-btn">
			                            <button onclick="cekDomain()" class="btn btn-warning" type="button">Cek Domain</button>
			                        </span>
			                    </div>
			                </div>
			                <script type="text/javascript">
			                    function cekDomain() {
			                        var pattern = "^[a-zA-Z0-9]+$";
			                        var nama_bisnis = document.getElementById("nama_bisnis").value;

			                        if (!nama_bisnis.match(pattern)) {
			                            alert("Nama toko tidak diizinkan. Mohon gunakan huruf dan angka saja, tanpa spasi.");
			                            return false;
			                        }

			                        $.ajax({
			                            data: {'domain':nama_bisnis},
			                            type:"GET",
			                            url: "http://yukbisnis.com/yubi/ajax_cek_domain/",
			                            success: function(data){
			                                $('#hasilDomain').html(data);
			                            }
			                        });
			                    }
			                </script>
			                <br>
			                <div class="control-group">
			                    <label>Tampilan Nama Bisnis</label>
			                    <div class="input-group">
			                        <span class="input-group-addon"><i class="fa fa-briefcase"></i></span>
			                        <input name="tampilan_nama" type="text" class="form-control" value="" required="">
			                    </div>
			                </div>
			                <br>
			                <div class="control-group">
			                    <label>Alamat Email</label>
			                    <div class="input-group">
			                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
			                        <input name="alamat_email" type="text" class="form-control" value="" required="">
			                    </div>
			                </div>
			                <br>
			                <div class="control-group">
			                    <label>Password</label>	                        
			                    <div class="input-group">
			                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
			                        <input name="password" type="password" class="form-control" value="" required="">
			                    </div>
			                </div>
			                <br>
			                <div class="control-group">
			                    <label>Ulangi Password</label>	                        
			                    <div class="input-group">
			                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
			                        <input name="ulangi_password" type="password" class="form-control" value="" required="">
			                    </div>
			                </div>
			                <br>
			                <legend>Data Pribadi</legend>
			                <div class="control-group">
			                    <label>Username <span class="small">(Contoh: MissYubi)</span></label>
			                    <div id="hasilUsername"></div>
			                    <div class="input-group">
			                        <span class="input-group-addon"><i class="fa fa-globe"></i></span>
			                        <input id="accountName" name="accountName" type="text" class="form-control" value="" required="">  
			                        <span class="input-group-btn">
			                            <button onclick="cekUsername()" class="btn btn-warning" type="button">Cek Username</button>
			                        </span>
			                    </div>
			                </div>
			                <br>


			                <script type="text/javascript">
			                    function cekUsername() {
			                        var pattern = "^[a-zA-Z0-9]+$";
			                        var accountName = document.getElementById("accountName").value;

			                        if (!accountName.match(pattern)) {
			                            alert("Username tidak diizinkan. Mohon gunakan huruf dan angka saja, tanpa spasi.");
			                            return false;
			                        }

			                        $.ajax({
			                            data: {'accountName':accountName},
			                            type:"GET",
			                            url: "http://yukbisnis.com/yubi/ajax_cek_username/",
			                            success: function(data){
			                                $('#hasilUsername').html(data);
			                            }
			                        });
			                    }
			                </script>

			                <div class="control-group">
			                    <label>Nama Lengkap</label>
			                    <div class="input-group">
			                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
			                        <input name="nama_lengkap" type="text" class="form-control" value="" required="">
			                    </div>
			                </div>
			                <br>

			                <button type="submit" class="btn btn-block btn-success"><i class="fa fa-ok-sign"></i> DAFTAR</button>
 
			        </div>
			        </div>
			    </form>
			</div>
		</div>
	</section>

<?php include "footer.php"; ?>